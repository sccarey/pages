# Pages (working title)
## Mission Statement

This project is to develop an alternative, user-input-driven book-meta-platform... "because Goodreads sucks".

## Development Roadmap

1. The ability to track reading progress that shows book completion progress.
1. A (better) search feature for finding books.
1. A (better) recommendation algorithm. Something like Spotify's
1. Ability to see what friends are reading.
